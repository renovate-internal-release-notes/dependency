# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [2.0.0](https://gitlab.com/renovate-internal-release-notes/application/compare/v1.0.0...v2.0.0) (2022-02-17)


### ⚠ BREAKING CHANGES

* add breaking change second file

### Features

* add breaking change second file ([b7a9772](https://gitlab.com/renovate-internal-release-notes/application/commit/b7a9772e69afa899ac5e111d635cc77596e63f64))

## 1.0.0 (2022-02-17)


### ⚠ BREAKING CHANGES

* add breaking change file

### Features

* add breaking change file ([e5ac799](https://gitlab.com/renovate-internal-release-notes/application/commit/e5ac7998c02b80557bf0ecc74694cb8b2debffd1))
* initial configuration ([6ffca75](https://gitlab.com/renovate-internal-release-notes/application/commit/6ffca7558f65cb7589c2344160a470eab9aa5020))

### 0.0.1 (2022-02-17)


### Features

* initial configuration ([6ffca75](https://gitlab.com/renovate-internal-release-notes/application/commit/6ffca7558f65cb7589c2344160a470eab9aa5020))

### 0.0.1 (2022-02-17)


### Features

* initial configuration ([6ffca75](https://gitlab.com/renovate-internal-release-notes/application/commit/6ffca7558f65cb7589c2344160a470eab9aa5020))
